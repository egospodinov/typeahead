const path = require('path');
const webpack = require('webpack');

module.exports={
    entry:'./index.js',
    output:{
        filename:'main.js',
        path:path.resolve(__dirname,'./dist')
    },
    module: {
        loaders: [
          {
            test: /\.js/,
            loader: ["source-map-loader",'babel-loader'],
            exclude: /node_modules/,
            query: {
                presets: ['es2015']
              }
           },
           {
            test: /\.css/,
            loaders: ['style', 'css'],
          }
        ],
        rules:[
            {
                test: /\.js$/,
                use: ["source-map-loader"],
                enforce: "pre"
              }
        ]
      },
      devServer: {
        host: 'localhost',
        port: 3000,
        historyApiFallback: true,
        contentBase: './',
        watchOptions: {
            aggregateTimeout: 100,
            poll: 300
        },
        hot:true,
        stats: {
            colors: true
        },
        open: true
    },
    plugins:[
        new webpack.SourceMapDevToolPlugin()
    ]
}