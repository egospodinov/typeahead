# ES6 Webpack Typeahead Component
The component can be initialized in two ways:
## Initialize only with html
### in your js file import the typeahead index:
```javascript
import './typeahead';
```
### in your html file:
```html
    <typeahead id="countries"
                label="Search countries:"
                min-symbols="2"
                url="https://mybookapi.eu/rest/v2/name/"
                list-wrapper="results.hits"
                path-to-title="book.title"
                path-to-subtitle="book.authors.0"
                path-to-image="book.cover.thumbnails.0"
    ></typeahead>
````
### HTML Attributes:

#### label(optional):
Sets a label to the input

#### min-symbols:
Sets the minimum number of symbols entered in the input before the Ajax request is triggered
if none is specified 3 is the default one.

### url:
The url to which the Ajax request will be made

### list-wrapper:
The list-wrapper is the key of the resulting Ajax JSONObject in which the array is contained.
Usualy when a an Ajax request for a list of item is done it comes back either as an array:
```javascript
[
    {
        title:'Title1',
        item:'Item1'
    },
     {
        title:'Title2',
        item:'Item2'
    }
]
```
or as an object containing an array:
```javascript
{
    results:{
        hits:{
            [
                {
                    title:'Title1',
                    item:'Item1'
                },
                 {
                    title:'Title2',
                    item:'Item2'
                }
            ]
        }
    }
}
```
In order to be able to point the component where exactly is the array we want we need the list-wrapper attribute. 
### path-to-title
The path to title attribute is a map that shows us where is the value we want to show as a title in the list item layout and respectively fill the input with when the list item is clicked.Given that we already pointed the path to the list in the JSON object then we need to point the path to the title. Say we've got the following list:
`````javascript
{
    results:{
        hits:{
            [
                {
                   country:"Germany",
                   book:{
                        title:"Meine lieblings Speisen",
                        author:"Max Musterman"
                   }
                },
                {
                    country:"United Kingdom"
                    book:{
                        title:"My favorite dishes",
                        author:"John Doe"
                   }
                }
            ]
        }
    }
}
``````
If we want in the title of every item in the suggestion list to be shown the title of the book we would set the path-to-title to:
```html
path-to-title="book.title"
``````
### path-to-subtitle
Same as the path-to-title attribute but for the second row (subtitle) of the list item

### path-to-image
Yes we can also show an image! In case there is an image url we can give the location in this attribute and it will be shown in the list item.

## How does it work?
Depending on what path(s)-to- attributes are given in the <typeahead> different templates of the listItem will be initialized and shown:
* one showing only title 
* one showing title and subtitle
* one showing image title and subtitle

## Initialize with js:
You can initialize the typeahead programatically aswell by following the example below:
```javascript
import './typeahead';
import Typeahead from './typeahead/js/typeahead';
import TypeaheadListManager from './typeahead/js/listTemplateManager';

const selectedAddress = document.getElementById('selected-address');
let renderer = new TypeaheadListManager();
//the custom listItemTempalte function
//this function must contain as parameters title and/or subtitle and/or image
/**
 also the root element of the list item should contain  data-title="${title}"
 so we can get the title string and put it as a value of the input
*/
let listItemTemplate = title => `<div class="custom-template" data-title="${title}"><span class="title">${title}</span></div>`;

//initiate a type ahead manualy by querying the element
let manualTypeahead = new Typeahead(document.getElementById('manual-typeahead'))
    //set the url for the ajax data request
    .setUrl('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCjm_5NIX5Jr_PGFVO5Te3EHYFPpPG26Qw&address=')
    //set the input label if desired
    .setLabel('Search Addres:')
    //callback when the results from the search are recieved
    //here we can get the listManager instance from the TypeAhead class instance e.g instance.listManager
    //format the data from the resultList as we wish 
    //and invoke the render method of the list manager and show the results
    .onResultsRecieved((resultList,instance)=>{
        let formatted  = resultList.results.map((item)=>{return {title:item.formatted_address}});
        instance.listManager.render(formatted,listItemTemplate);
    })
    .onItemClicked((value)=>{
        selectedAddress.innerHTML = "Selected address: <strong>"+value+"</strong>";
    })
    .initialize();
```