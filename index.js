import './typeahead';
import Typeahead from './typeahead/js/typeahead';
import TypeaheadListManager from './typeahead/js/listTemplateManager';

const selectedAddress = document.getElementById('selected-address');
let renderer = new TypeaheadListManager();
//the custom listItemTempalte function
//this function must contain as parameters title and/or subtitle and/or image
/**
 also the root element of the list item should contain  data-title="${title}"
 so we can get the title string and put it as a value of the input
*/
let listItemTemplate = title => `<div class="custom-template" data-title="${title}"><span class="title">${title}</span></div>`;

//initiate a type ahead manualy by querying the element
let manualTypeahead = new Typeahead(document.getElementById('manual-typeahead'))
    //set the url for the ajax data request
    .setUrl('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCjm_5NIX5Jr_PGFVO5Te3EHYFPpPG26Qw&address=')
    //set the input label if desired
    .setLabel('Search Addres:')
    //callback when the results from the search are recieved
    //here we can get the listManager instance from the TypeAhead class instance e.g instance.listManager
    //format the data from the resultList as we wish 
    //and invoke the render method of the list manager and show the results
    .onResultsRecieved((resultList,instance)=>{
        let formatted  = resultList.results.map((item)=>{return {title:item.formatted_address}});
        instance.listManager.render(formatted,listItemTemplate);
    })
    .onItemClicked((value)=>{
        selectedAddress.innerHTML = "Selected address: <strong>"+value+"</strong>";
    })
    .initialize();
