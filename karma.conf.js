module.exports = function(config) {
    config.set({
        browsers: ['PhantomJS'],
        files: [
            { pattern: 'test-context.js', watched: false }
        ],
        plugins:['karma-jasmine','karma-phantomjs-launcher','karma-webpack'],
        frameworks: ['jasmine'],
        preprocessors: {
            'test-context.js': ['webpack']
        },
        autoWatch: true,
        webpack: {
            module: {
                loaders: [
                    {
                        test: /\.js/,
                        loader: 'babel-loader',
                        exclude: /node_modules/,
                        query: {
                            presets: ['es2015']
                          }
                       }
                ]
            },
            watch: true
        },
        webpackServer: {
            noInfo: true
        }
    });
};